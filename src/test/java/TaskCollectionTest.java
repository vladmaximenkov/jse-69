import marker.ApiCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.vmaksimenkov.tm.model.Task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static ru.vmaksimenkov.tm.constant.Const.TASKS_URL;

public class TaskCollectionTest extends AbstractTest {

    @NotNull
    private final Task task1 = new Task("test1");

    @NotNull
    private final Task task2 = new Task("test2");

    @NotNull
    private final List<Task> tasks = new ArrayList<>();

    {
        tasks.add(task1);
        tasks.add(task2);
    }

    @BeforeClass
    public static void beforeClass() {
        auth();
    }

    private void post(@NotNull final List<Task> tasks) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<List<Task>> httpEntity = new HttpEntity<>(tasks, getHeader());
        restTemplate.postForEntity(TASKS_URL, httpEntity, Task.class);
    }

    private void put(@NotNull final List<Task> tasks) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<List<Task>> httpEntity = new HttpEntity<>(tasks, getHeader());
        restTemplate.put(TASKS_URL, httpEntity);
    }

    @Nullable
    private Collection<Task> get() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Task> httpEntity = new HttpEntity<>(getHeader());
        @NotNull final ResponseEntity<Collection> response = restTemplate.exchange(
                TASKS_URL, HttpMethod.GET, httpEntity, Collection.class
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        return response.getBody();
    }

    @Nullable
    private Task deleteAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Task> httpEntity = new HttpEntity<>(getHeader());
        @NotNull final ResponseEntity<Task> response = restTemplate.exchange(
                TASKS_URL, HttpMethod.DELETE, httpEntity, Task.class
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        return response.getBody();
    }

    @Test
    @Category(ApiCategory.class)
    public void postTest() {
        deleteAll();
        post(tasks);
        @Nullable final Collection<Task> taskResponse = get();
        Assert.assertNotNull(taskResponse);
        Assert.assertEquals(2, taskResponse.size());
    }

    @Test
    @Category(ApiCategory.class)
    public void putTest() {
        task1.setName("Put 1");
        task2.setName("Put 2");
        tasks.clear();
        tasks.add(task1);
        tasks.add(task2);
        put(tasks);
        @Nullable final Collection<Task> tasksNew = get();
        Assert.assertNotNull(tasksNew);
        final ArrayList<Object> tasks = (ArrayList) tasksNew;
        Assert.assertTrue(tasks.get(0).toString().contains("Put 1") || tasks.get(0).toString().contains("Put 2"));
    }

    @Test
    @Category(ApiCategory.class)
    public void getTest() {
        post(tasks);
        @Nullable final Collection<Task> tasksNew = get();
        Assert.assertNotNull(tasksNew);
    }

    @Test
    @Category(ApiCategory.class)
    public void deleteTest() {
        post(tasks);
        deleteAll();
        Assert.assertEquals(0, get().size());
    }

}

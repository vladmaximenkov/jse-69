package ru.vmaksimenkov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.vmaksimenkov.tm.dto.TaskRecord;

import java.util.List;

public interface TaskRecordRepository extends AbstractBusinessRecordRepository<TaskRecord> {

    @Modifying
    @Query("UPDATE TaskRecord SET projectId = :projectId WHERE userId = :userId AND id = :taskId")
    void bindTaskPyProjectId(@NotNull @Param("userId") String userId, @Nullable @Param("projectId") String projectId, @Nullable @Param("taskId") String taskId);

    @Nullable List<TaskRecord> findAllByUserIdAndProjectId(@NotNull @Param("userId") String userId, @Nullable @Param("projectId") String projectId);

    Long deleteByUserIdAndProjectId(@NotNull @Param("userId") String userId, @Nullable @Param("projectId") String projectId);

    Long deleteByUserIdAndProjectIdIsNotNull(@NotNull @Param("userId") String userId);

    @Nullable TaskRecord findByUserIdAndName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    boolean existsByUserIdAndName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    void deleteByUserId(@NotNull @Param("userId") String userId);

    void deleteByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    boolean existsByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable List<TaskRecord> findAllByUserId(@NotNull @Param("userId") String userId);

    @Nullable TaskRecord findByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @NotNull Long countByUserId(@NotNull @Param("userId") String userId);

}

package ru.vmaksimenkov.tm.api.resource;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.dto.TaskRecord;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RestController
@RequestMapping("/api/tasks")
public interface ITaskCollectionResource {

    @NotNull
    @WebMethod
    @GetMapping
    Collection<TaskRecord> get();

    @WebMethod
    @PostMapping
    void post(@NotNull @WebParam(name = "tasks") @RequestBody List<TaskRecord> tasks);

    @WebMethod
    @PutMapping
    void put(@NotNull @WebParam(name = "tasks") @RequestBody List<TaskRecord> tasks);

    @WebMethod
    @DeleteMapping
    void delete();

}

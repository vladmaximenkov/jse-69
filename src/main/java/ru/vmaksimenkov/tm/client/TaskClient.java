package ru.vmaksimenkov.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.client.RestTemplate;
import ru.vmaksimenkov.tm.api.resource.ITaskCollectionResource;
import ru.vmaksimenkov.tm.api.resource.ITaskResource;
import ru.vmaksimenkov.tm.dto.TaskRecord;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ru.vmaksimenkov.tm.constant.Const.TASKS_URL;
import static ru.vmaksimenkov.tm.constant.Const.TASK_URL;

public class TaskClient implements ITaskResource, ITaskCollectionResource, TaskClientFeign {

    public void post(@NotNull final List<TaskRecord> tasks) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForEntity(TASKS_URL, tasks, void.class);
    }

    @NotNull
    public List<TaskRecord> get() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @Nullable final TaskRecord[] tasks = restTemplate.getForObject(TASKS_URL, TaskRecord[].class);
        if (tasks == null) return new ArrayList<>();
        return Arrays.asList(tasks);
    }

    public void put(@NotNull final List<TaskRecord> tasks) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.put(TASKS_URL, tasks, void.class);
    }

    public void delete() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(TASKS_URL);
    }

    public void post(@NotNull final TaskRecord task) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForEntity(TASK_URL, task, void.class);
    }

    @Nullable
    public TaskRecord get(@NotNull final String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(TASK_URL + "/{id}", TaskRecord.class, id);
    }

    public void put(@NotNull final TaskRecord task) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.put(TASK_URL, task, void.class);
    }

    public void delete(@NotNull final String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(TASK_URL + "/{id}", id);
    }

}

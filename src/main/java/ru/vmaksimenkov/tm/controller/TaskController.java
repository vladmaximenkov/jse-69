package ru.vmaksimenkov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.vmaksimenkov.tm.dto.TaskRecord;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.model.CustomUser;
import ru.vmaksimenkov.tm.service.TaskRecordService;

@Controller
public class TaskController {

    @Autowired
    private TaskRecordService service;

    @GetMapping("/task/create")
    @PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
    public String create(@AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user) {
        @NotNull final TaskRecord task = new TaskRecord();
        task.setUserId(user.getId());
        service.merge(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    @PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
    public String delete(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user,
            @PathVariable("id") String id
    ) {
        service.removeByUserIdAndId(user.getId(), id);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    @PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
    public ModelAndView edit(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user,
            @PathVariable("id") String id
    ) {
        @Nullable final TaskRecord task = service.findByUserIdAndId(user.getId(), id);
        return new ModelAndView("task-edit", "task", task);
    }

    @PostMapping("/task/edit/{id}")
    @PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
    public String edit(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user,
            @ModelAttribute("task") TaskRecord task,
            BindingResult result
    ) {
        task.setUserId(user.getId());
        service.merge(task);
        return "redirect:/tasks";
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

}
